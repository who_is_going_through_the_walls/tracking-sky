﻿namespace Common
{
    internal class Constants
    {
        public const string EntityFrameworkConnectionString = "EntityFrameworkConnectionString";
        public const string NHibernateConnectionString = "NHibernateConnectionString";
        public const string NHibernateSchemaUpdate = "NHibernateSchemaUpdate";
    }
}
