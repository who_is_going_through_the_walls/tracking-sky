﻿using System;

namespace Common.Attributes
{
    /// <summary>
    /// Attribute is using to mark property as not mapped
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DoNotMapAttribute : Attribute
    {
    }
}
