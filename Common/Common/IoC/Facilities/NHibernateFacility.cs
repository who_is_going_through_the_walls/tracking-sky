﻿using Castle.Core.Internal;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Common.Attributes;
using Data.Interfaces;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;

namespace Common.IoC.Facilities
{
    public class NHibernateFacility : AbstractFacility
    {
        protected string connectionString;
        protected string nHibernateSchemaUpdate;

        protected override void Init()
        {
            var config = BuildDatabaseConfiguration();
            Kernel.Register(
                Component.For<ISessionFactory>().UsingFactoryMethod(kernel => config.BuildSessionFactory()),                //register NHibernate session factory by using NHibernate Configuration
                Component.For<ISession>().UsingFactoryMethod(kernel => kernel.Resolve<ISessionFactory>().OpenSession())     //register session by using Castle Windsor kernel to resolve session factory
                    .LifestylePerWebRequest());
        }

        internal void Config(string connectionString, string nHibernateSchemaUpdate)
        {
            this.connectionString = connectionString;
            this.nHibernateSchemaUpdate = nHibernateSchemaUpdate;
        }

        private NHibernate.Cfg.Configuration BuildDatabaseConfiguration()
        {
            return Fluently.Configure()
                .Database(SetupDatabase)
                //.ProxyFactoryFactory(typeof(ProxyFactoryFactory))
                .Mappings(m => m.AutoMappings.Add(CreateMappingModel))
                //.Mappings(m => m.FluentMappings.Add(typeof(Entity))
                //    .Add<Record>()
                //    .Add<RecordExt>())
                .ExposeConfiguration(ConfigurePersistence)
                .BuildConfiguration();
        }

        protected virtual void ConfigurePersistence(NHibernate.Cfg.Configuration config)
        {
            SchemaMetadataUpdater.QuoteTableAndColumns(config);

            //Update or create database schema:
            switch (nHibernateSchemaUpdate)
            {
                case "Update":
                    new SchemaUpdate(config).Execute(false, true);
                    break;
                case "Create":
                    var exportUtil = new SchemaExport(config);
                    exportUtil.Drop(false, true);
                    exportUtil.Create(false, true);
                    break;
            }
        }

        protected virtual AutoPersistenceModel CreateMappingModel()
        {
            var autoPersistanceModel = AutoMap.Assembly(typeof(IData).Assembly)
                //.Setup(x => x.FindMembers()
                //.Alterations(x => x.Add<AutoMappingAlterationCollection>)
                .Where(IsDomainEntity)
                .OverrideAll(ShouldIgnoreProperty)
                //.SetLogger(FluentNHibernate.Diagnostics.IDiagnosticLogger)        //TODO: investigate using for logging
                .Conventions.Add(DefaultCascade.All())
                .IgnoreBase<IData>();

            return autoPersistanceModel;
        }

        protected virtual bool IsDomainEntity(Type t)
        {
            return typeof(IData).IsAssignableFrom(t);
        }

        protected virtual IPersistenceConfigurer SetupDatabase()
        {
            return MsSqlConfiguration.MsSql2012
                .UseOuterJoin()
                .ConnectionString(connectionString)
                //.ConnectionString(x => x.FromConnectionStringWithKey(connectionStringName))
                .ShowSql();
        }

        private void ShouldIgnoreProperty(IPropertyIgnorer property)
        {
            property.IgnoreProperties(p => p.MemberInfo.HasAttribute<DoNotMapAttribute>());
        }
    }
}
