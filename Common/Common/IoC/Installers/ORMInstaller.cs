﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Common.IoC.Facilities;
using System.Configuration;

namespace Common.IoC.Installers
{
    public class ORMInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //TODO: need to move all ConfigurationManager connection strings/app settings etc. configuration options to some common class, for example: AppConfiguration
            container
                .AddFacility<EntityFrameworkFacility>(facility => facility.Config(
                    ConfigurationManager.ConnectionStrings[Constants.EntityFrameworkConnectionString].ConnectionString))
                .AddFacility<NHibernateFacility>(facility => facility.Config(
                    ConfigurationManager.ConnectionStrings[Constants.NHibernateConnectionString].ConnectionString,
                    ConfigurationManager.AppSettings[Constants.NHibernateSchemaUpdate]));

        }
    }
}
