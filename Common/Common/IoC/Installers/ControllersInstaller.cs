﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Common.IoC.Installers
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //Register our controllers in Winsor:
            container.Register(
                //Classes.FromThisAssembly()
                Classes.FromAssemblyNamed("Mvc")    //assembly with controller classes
                .BasedOn<IController>()
                .LifestyleTransient()//, //Transient. Set up instance lifestyle as one per request what's MVC framework expects. New controller instance will be provided by Windsor every time it's needed
                //Component.For<>
            );

            //container.Kernel.Register(...);
        }
    }
}