﻿using Data;
using EntityFrameworkORM;
using NHibernate;
using System;
using System.Web.Mvc;

namespace Mvc.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IRepository<Entity> repository;
        private readonly ISession session;

        public HomeController(/*IDbContext context*/IRepository<Entity> repository, ISession session)
        {
            this.repository = repository;
            this.session = session;
        }

        public ActionResult Index()
        {
            Log.Warn("HomeController: Index action processing");

            repository.Create(new Entity
            {
                Id = Guid.NewGuid(),
                Message = "Test message"
            });

            using (var transaction = session.BeginTransaction())
            {
                var entity = new Entity {
                    //Id = Guid.NewGuid(),
                    Message = "NHibernate test message"
                };
                session.SaveOrUpdate(entity);
                transaction.Commit();
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}