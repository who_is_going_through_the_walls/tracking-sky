﻿using Castle.Core.Logging;
using System.Web.Mvc;

namespace Mvc.Controllers
{
    public abstract class BaseController : Controller
    {
        // this is Castle.Core.Logging.ILogger, not log4net.Core.ILogger
        public ILogger Log { get; set; }        //Castle.Windsor able to do Property injection, we use NullLogger.Instance in this controller constructor. But because of Property injection after controller would be constructed, Castle Windsor will assign the REAL logger to this Log property

        public BaseController(/*ILogger log*/)
        {
            //Log = log;

            Log = NullLogger.Instance;      //doesn't except but doesn't log any info also(if Log property would be private!!!). It will work only if Log property declared as public. 
                                            //This is how it works - http://hwyfwk.com/blog/2013/10/06/nlog-and-castle-logging-facility-for-logging/
                                            //1. Declare a public property of type ILogger
                                            //2. Initialize that property in your constructor to NullLogger.Instance
                                            //This uses the ability of Castle.Windsor to do Property injection, so that after you class is constructed Castle Windsor will assign the real logger to that Logger property. 
                                            //But, if you ever remove the LoggingInstaller your code will continue to work because the NullLogger is a proper implementation of ILogger which does nothing. 
                                            //This avoids NullReferenceExceptions when you don’t have a Logger injected, for instance during tests.


            //To use into Watch window(if using NLog):
            //((NLog.Targets.FileTarget)(((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration.ConfiguredNamedTargets[0])).FileName;
            //((NLog.Layouts.SimpleLayout)((NLog.Targets.FileTarget)(((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration.ConfiguredNamedTargets[0])).FileName).FixedText;
            //((NLog.Layouts.SimpleLayout)((NLog.Targets.TargetWithLayoutHeaderAndFooter)(((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration.ConfiguredNamedTargets[0])).Layout).Text;
            //(((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration).Variables;
            //new System.Collections.Generic.Mscorlib_CollectionDebugView<NLog.Config.LoggingRule>((((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration).LoggingRules).Items[0].Levels;
            //new System.Collections.Generic.Mscorlib_DictionaryKeyCollectionDebugView<string, bool>(((NLog.Config.XmlLoggingConfiguration)(((Castle.Services.Logging.NLogIntegration.NLogLogger)Log).Logger.Factory.Configuration)).FileNamesToWatch).Items[0];
        }
    }
}
