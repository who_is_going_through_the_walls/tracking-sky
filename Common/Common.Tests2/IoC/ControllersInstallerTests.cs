﻿using Castle.Windsor;
using Xunit;
using Common.Tests.Helpers;
using System.Web.Mvc;
using Castle.Core.Internal;
using Common.IoC.Installers;

namespace Common.Tests.IoC
{
    public class ControllersInstallerTests
    {
        private IWindsorContainer containerWithControllers;

        /// <summary>
        /// Set up environment
        /// </summary>
        public ControllersInstallerTests()
        {
            containerWithControllers = new WindsorContainer()
                .Install(new ControllersInstaller());
        }

        /// <summary>
        /// that all types implementing IController are registered and that ONLY types implementing IController are registered
        /// </summary>
        [Fact]
        public void All_controllers_implement_IController()
        {
            var allHandlers = containerWithControllers.GetAllHandlers();
            var controllerHandlers = typeof(IController).GetHandlersFor(containerWithControllers);

            Assert.NotEmpty(allHandlers);
            Assert.Equal(allHandlers, controllerHandlers);
        }

        /// <summary>
        /// verify that indeed all types implementing IController from our application assembly are registered by the ControllersInstaller
        /// </summary>
        [Fact]
        public void All_controllers_are_registered()
        {

            // Is<TType> is an helper, extension method from Windsor in the Castle.Core.Internal namespace
            // which behaves like 'is' keyword in C# but at a Type, not instance level
            var allControllers = TypeHelper.GetControllersFromApplicationAssembly(c => c.Is<IController>());
            var registeredControllers = typeof(IController).GetImplementationTypesFor(containerWithControllers);
            Assert.Equal(allControllers, registeredControllers);
        }
    }
}