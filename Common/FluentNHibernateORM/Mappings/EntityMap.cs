﻿using Data;
using FluentNHibernate.Mapping;
using System;

namespace FluentNHibernateORM.Mappings
{
    public class EntityMap : ClassMap<Entity>
    {
        //http://stackoverflow.com/questions/21042663/how-to-resolve-batch-update-returned-unexpected-row-count-error-in-nhibernate
        public EntityMap() {
            Id(x => x.Id).UnsavedValue(new Guid("00000000-0000-0000-0000-000000000000"));
            Map(x => x.Message);
        }
    }
}
