﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Mvc.Controllers;
using EntityFrameworkORM;
using Data;
using NHibernate;
using Moq;

namespace Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        IDbContext fakeContext;
        IRepository<Entity> fakeRepository;
        ISession Session {
            get {
                var sessionMock = new Mock<ISession>();
                sessionMock.Setup(x => x.SaveOrUpdate(It.IsAny<object>()));
                return sessionMock.Object;
            }
        }
        HomeController controller;

        [TestInitialize]
        public void Init()
        {
            //Need to mock:
            fakeContext = new DataContext("test");      //because of wrong connection string it will cause error in tests. Need to mock context and repository further to fix
            fakeRepository = new DataRepository<Entity>(fakeContext);

            // Arrange
            controller = new HomeController(fakeRepository, Session);
        }

        [TestMethod]
        public void Index()
        {
            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
