﻿using Data;
using System.Data.Entity;

namespace EntityFrameworkORM
{
    public class DataContext : DbContext, IDbContext
    {
        private DataContext() { }
        public DataContext(string connectionString) : base(connectionString) {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DataContext>());
            Configuration.ProxyCreationEnabled = false;
        }

        /// <summary>
        /// Hide nested DbSet<TEntity> Set<TEntity>() and use IDbSet<TEntity> Set<TEntity>() instead of
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>Set of entities</returns>
        public IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity> ();
        }

        /// <summary>
        /// Mapping business entities to appropriate tables into Sql Server (if use code first approach it will help to create tables in database which will based on your business entities)
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Mappings for entities:

            modelBuilder.HasDefaultSchema("EF");    //set default EF schema for model builder

            //First approach - mapping one entity to one table in db
            modelBuilder.Entity<Entity>().ToTable("Entity");
            modelBuilder.Entity<Record>().ToTable("Record");
            modelBuilder.Entity<RecordExt>().ToTable("RecordExt");


            //Second approach - 
            //modelBuilder.Entity<Student>().Map(m =>
            //{
            //    m.Properties(p => new { p.StudentId, p.StudentName });
            //    m.ToTable("StudentInfo", "StudentsSchema");      //set StudentsSchema for this kind of table

            //}).Map(m => {
            //    m.Properties(p => new { p.StudentId, p.Height, p.Weight, p.Photo, p.DateOfBirth });
            //    m.ToTable("StudentInfoDetail", "StudentsSchema");   //set StudentsSchema for this kind of table

            //});

            //Third approach - registering EntityTypeConfiguration<TEntity>(which configures mapping entity to table inside itself) in model bilder configurations.
            //This approach is the most convinient when you need to do precise configuration of db table 
            //public class ProductMap : EntityTypeConfiguration<Product>
            //{
            //    public ProductMap()
            //    {
            //        ToTable("Product");
            //        HasKey(p => p.Id).Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //        //CategoryId as foreign key
            //        HasRequired(p => p.Category)
            //            .WithMany(c => c.Products)
            //            .HasForeignKey(p => p.CategoryId);
            //        Property(p => p.Name).IsRequired().HasMaxLength(100);
            //        Property(p => p.MinimumStockLevel);
            //    }
            //}
            //modelBuilder.Configurations.Add(new ProductMap());


            base.OnModelCreating(modelBuilder);
        }
    }
}
