﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace EntityFrameworkORM
{
    public class DataRepository<T> : IRepository<T> where T : class
    {
        private IDbContext context;

        private DataRepository() { }

        public DataRepository(IDbContext context) {
            this.context = context;
        }

        private IDbSet<T> ContextSet
        {
            get { return context.Set<T>(); }
        }

        public IQueryable<T> All()
        {
            return ContextSet.AsQueryable();
        }

        public bool Contains(Expression<Func<T, bool>> predicate)
        {
            return All().Count(predicate) > 0;
        }

        public void Create(T item)
        {
            ContextSet.Add(item);
            context.SaveChanges();
        }

        public int Delete(Expression<Func<T, bool>> predicate)
        {
            Filter(predicate).ToList().ForEach(x => ContextSet.Remove(x));
            return context.SaveChanges();
        }

        public int Delete(T item)
        {
            ContextSet.Remove(item);
            return context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
                context.Dispose();
        }

        public void ExecuteProcedure(string sql, params SqlParameter[] sqlParams)
        {
            context.Database.ExecuteSqlCommand(sql, sqlParams);
        }

        public IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
        {
            return All().Where(predicate).AsQueryable<T>();
        }

        public IQueryable<T> Filter(Expression<Func<T, bool>> predicate, out int total, int index = 0, int size = 50)
        {
            var skipCount = index * size;
            var entities = Filter(predicate);
            entities = skipCount == 0 ? entities.Take(size) : entities.Skip(skipCount).Take(size);
            total = entities.Count();
            return entities;
        }

        public T Find(params object[] keys)
        {
            return ContextSet.Find(keys);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            return All().First(predicate);
        }

        public T Single(Expression<Func<T, bool>> expression)
        {
            return All().Single(expression);
        }

        public int Update(T item)
        {
            //try
            //{
                var entry = context.Entry(item);
                ContextSet.Attach(item);
                entry.State = EntityState.Modified;
                return SaveChanges();
            //}
            //catch (OptimisticConcurrencyException ex)
            //{
            //    throw ex;
            //}
        }
    }
}
