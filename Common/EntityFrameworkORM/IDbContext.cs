﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace EntityFrameworkORM
{
    /// <summary>
    /// This interface contains only those members of class DbContext which would be used by application. So one of the reasons of using interface it's hidding 
    /// DbContext members which won't be used. Second reason of using this interface it's possibility to mock DbContext implementation through the interface
    /// </summary>
    public interface IDbContext : IDisposable
    {
        Database Database { get; }
        DbEntityEntry Entry(object entity);
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
    }
}
