﻿using System;

namespace Data.Interfaces
{
    public interface IData
    {
        Guid Id { get; set; }
    }
}
