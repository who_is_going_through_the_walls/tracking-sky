﻿using Data.Interfaces;
using System;

namespace Data
{
    public class Record : IData
    {
        public virtual Guid Id { get; set; }
        public virtual string Message { get; set; }
    }
}
