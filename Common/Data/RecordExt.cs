﻿using Data.Interfaces;
using System;

namespace Data
{
    public class RecordExt : IData
    {
        public virtual Guid Id { get; set; }
        public virtual DateTime Date { get; set; }
    }
}
