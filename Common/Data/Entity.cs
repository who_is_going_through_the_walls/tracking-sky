﻿using Data.Interfaces;
using System;

namespace Data
{
    public class Entity : IData
    {
        public virtual Guid Id { get; set; }
        public virtual string Message { get; set; }
    }
}
