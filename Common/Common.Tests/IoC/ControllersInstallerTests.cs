﻿using Castle.Windsor;
using Common.IoC;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.Tests.Helpers;
using System.Web.Mvc;
using Castle.Core.Internal;
using System.Collections.Generic;
using Castle.MicroKernel;
using System.Linq;
using Castle.Core;
using Common.IoC.Installers;

namespace Common.Tests.IoC
{
    [TestClass]
    public class ControllersInstallerTests
    {
        private IWindsorContainer containerWithControllers;

        /// <summary>
        /// Set up environment
        /// </summary>
        public ControllersInstallerTests()
        {
            containerWithControllers = new WindsorContainer()
                .Install(new ControllersInstaller());
        }

        /// <summary>
        /// that all types implementing IController are registered and that ONLY types implementing IController are registered
        /// </summary>
        [TestMethod]
        public void All_controllers_implement_IController()
        {
            var allHandlers = containerWithControllers.GetAllHandlers();
            //Castle.MicroKernel.Handlers.DefaultHandler handler = (Castle.MicroKernel.Handlers.DefaultHandler)allHandlers[0];      //handler.ComponentModel property is invisible into debug mode and into Watch windows because of [DebuggerBrowsableAttribute]
            //var componentModel = handler.ComponentModel;
            var controllerHandlers = typeof(IController).GetHandlersFor(containerWithControllers);

            Assert.IsNotNull(allHandlers);
            Assert.IsTrue(allHandlers.Length > 0);
            CollectionAssert.AreEqual(allHandlers, controllerHandlers);
        }

        /// <summary>
        /// verify that indeed all types implementing IController from our application assembly are registered into Windsor container
        /// </summary>
        [TestMethod]
        public void All_controllers_are_registered()
        {

            // Is<TType> is an helper, extension method from Windsor in the Castle.Core.Internal namespace
            // which behaves like 'is' keyword in C# but at a Type, not instance level
            var allControllers = TypeHelper.GetTypesFromApplicationAssembly(c => c.Is<IController>());
            var registeredControllers = typeof(IController).GetImplementationTypesFor(containerWithControllers);
            CollectionAssert.AreEqual(allControllers, registeredControllers);
        }

        [TestMethod]
        public void All_and_only_controllers_have_Controllers_suffix()
        {
            var allControllers = TypeHelper.GetTypesFromApplicationAssembly(c => c.Name.EndsWith("Controller"));
            var registeredControllers = typeof(IController).GetImplementationTypesFor(containerWithControllers);
            CollectionAssert.AreEqual(allControllers, registeredControllers);
        }

        [TestMethod]
        public void All_and_only_controllers_live_in_Controllers_namespace()
        {
            var allControllers = TypeHelper.GetTypesFromApplicationAssembly(c => c.Namespace.Contains("Controllers"));
            var registeredControllers = typeof(IController).GetImplementationTypesFor(containerWithControllers);
            CollectionAssert.AreEqual(allControllers, registeredControllers);
        }

        [TestMethod]
        public void All_controllers_are_transient()
        {
            var nonTransientControllers = typeof(IController).GetHandlersFor(containerWithControllers)
                .Where(controller => controller.ComponentModel.LifestyleType != LifestyleType.Transient)
                .ToArray();

            Assert.IsNotNull(nonTransientControllers);
            Assert.IsTrue(nonTransientControllers.Length == 0);
        }

        [TestMethod]
        public void All_controllers_expose_themselves_as_service()
        {
            var controllersWithWrongName = typeof(IController).GetHandlersFor(containerWithControllers)
                .Where(controller => controller.ComponentModel.Services.Single() != controller.ComponentModel.Implementation)   //checks one controller doesn't represent itself as several services(controllers - etc. base controller and two inherited controllers of it) and checks it has the same implementation(etc. base controller by some reason would be represented as inherited controller)
                .ToArray();

            Assert.IsNotNull(controllersWithWrongName);
            Assert.IsTrue(controllersWithWrongName.Length == 0);
        }
    }
}